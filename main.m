clear all;close all; clc;
%% Initiallization - Arrays
ite = 1; % index that is used for the rng function, in order to create the same random data
         % at each instance of execution. Essentially, we need it in order our
         % comparisons between the cases of different location areas to be
         % more accurate.
la_area = 2;
bts_area = 1;
bts_per_area = (la_area/bts_area)^2 ; % number of bts inside a location area

rng(ite); ite = ite+1;
cord = 10*rand(2,5000); % mobile users' coordinates

rng(ite); ite = ite+1; 
dest = 10*rand(2,5000); % mobile users' destination points
trip = sqrt(sum((cord - dest) .^ 2)); % total distance that mobile user has to travel
slopes = (dest(2,:) - cord(2,:)) ./ (dest(1,:) - cord(1,:)); % slope of the line

rng(ite); ite = ite+1;
speed = .01*rand(1,5000); % speed of the mobile user randomly choosen in [0, 0.01 km/s] 
la_id = zeros(1,5000); % local area id where mobile users belongs
for u = 1:5000
    la_id(u) = la_find(cord(1,u),cord(2,u));
end
%%
location_traffic = zeros(1,3600);

paging_traffic = zeros(1,3600);

boarder_passes = zeros(1,3600);

for t = 1:3600 % (1 hour duration)
    % Calls and paging traffic
    rng(ite); ite = ite+1;
    total_calls = poissrnd(0.0125*5000,1,1); % based on the poisson distribution
    paging_traffic(t) = total_calls * 60 * bts_per_area;
    
    for u = 1:5000
        trip(u) = trip(u) - speed(u); % the remaining trip for the user
        if(trip(u)<=0) % the mobile user reached his destination
            cord(1,u) = dest(1,u); cord(2,u) = dest(2,u);
            rng(ite); ite = ite+1;
            dest(:,u) = 10*rand(2,1); % a new destination point is generated
            slopes(u) = (dest(2,u) - cord(2,u)) ./ (dest(1,u) - cord(1,u)); % the new slope
            trip(u) = sqrt(sum((cord(:,u) - dest(:,u)) .^ 2)); % the new trip
        else % the user keeps moving on his line
            cord(1,u) = cord(1,u) + speed(u)*cos(slopes(u)); % movement on the x-coordinate
            cord(2,u) = cord(2,u) + speed(u)*sin(slopes(u)); % movement on the y-coordinate
        end    
        new_la_id = la_find(cord(1,u),cord(2,u));
        if(new_la_id ~= la_id(u)) % checking if the mobile user entered a new location area
            location_traffic(t) = location_traffic(t) + 800; % traffic in kbps
            la_id(u) = new_la_id;
            boarder_passes(t) = boarder_passes(t) + 1;
        end
        rng(ite); ite = ite+1;
        speed(u) = .01*rand(1,1); % new speed
    end 
end
total_board = sum(boarder_passes);
total_loc_traf = sum(location_traffic)/10^6; % Gbps
total_pag_traf = sum(paging_traffic)/10^6; % Gbps


agg_loc_traf = zeros(1,3600);
agg_pag_traf = zeros(1,3600);
for t = 1:3600
    agg_loc_traf(t)= sum(location_traffic(1:t));
    agg_pag_traf(t) = sum(paging_traffic(1:t));
end
agg_loc_traf = agg_loc_traf / 10^6;
agg_pag_traf = agg_pag_traf / 10^6;







