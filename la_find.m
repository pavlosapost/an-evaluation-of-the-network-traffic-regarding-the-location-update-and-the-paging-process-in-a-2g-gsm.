% This function takes as input the coordinates of the mobile user and
% returns the id of the location area where the mobile user belongs. For example,
% in the case where each location area is 2*2 km the ids are numbers between [1,25]
function la_id = la_find(x,y)

las = 5; % is the number of Location Areas in a row of the grid which is 10*10 km
         % In the case where the location area is 3*3 km, we have to take
         % under consideration that in a row exist 4 different location
         % areas, as the fourth one it starts from 9th km and ends at the
         % 12th km which is outside of our grid.
las_size = 2; % the size of the local area
checkx = mod(x,1);
checky = mod(y,1);

if(checkx == 0 && checky == 0)
    la_id = fix((y-1)/las_size)*las + fix((x-1)/las_size) + 1;
else
    if(checkx==0)
        la_id = fix(y/las_size)*las + fix((x-1)/las_size) + 1;
    else
        if(checky==0)
            la_id = fix((y-1)/las_size)*las + fix(x/las_size) + 1;
        else
            la_id = fix(y/las_size)*las + fix(x/las_size) + 1;
        end
    end
end

